<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(':checkbox:checked').prop('checked',false);
    $("#make_it_premium").click(function(){
	  if ($('input#make_it_premium').is(':checked')) {								 
		var price = $("#v_price").val();
		var v_price = Number(price)+Number(price/2);
		v_price = v_price.toFixed(2);		
        $('.price').text("$"+v_price);
		$('input.surcharge_p').val(Number(price/2));;
	  }else {
		 var price = $("#v_price").val();
		 price = Number(price).toFixed(2);
        $('.price').text("$"+price);
	  }
    });
});
</script>

<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<p class="price"><?php echo $product->get_price_html(); ?></p>
     <input type="hidden" value="<?php echo $product->regular_price;?>" name="v_price" id="v_price"/>
    
	<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
